# AstroStack
### A java application for plate solving, sorting, and viewing astrophotography images.
This software uses the [Astrometry.net API](https://astrometrynet.readthedocs.io/en/latest/net/api.html) to plate solve the images.
Images can then be sorted by various astronomical catalogues (Messier, NGC, Caldwell, Herschel).

The software could be useful for Astrophotographers in the field, collectors looking to image every object in a catalogue, or hobbyists looking for a neat way to catagorise and display their astrophotographs.


