import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Properties;
import java.util.prefs.Preferences;
import org.json.*;


public class Home{

    JMenuBar mainMenu;
    Preferences prefs;
    String IMAGE_DIRECTORY = "";
    private static final int SIZE = 250;
    private static final int GAP = 10;
    private static final String apiKey = "viktqrhlnwfrnzvr";
    private boolean cals = false;

    public Home(String title){
        JFrame home = new JFrame();
        home.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        home.setTitle(title);
        GridLayout grid = new GridLayout(0,2);
        grid.setHgap(10);
        grid.setVgap(10);
        Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
        home.setSize(screen);

        mainMenu = new MenuBar();
        home.setJMenuBar(mainMenu);

        prefs = Preferences.userNodeForPackage(Home.class);
        System.out.println("Preferences say images can be found at " + prefs.get(IMAGE_DIRECTORY, null));
        //get files at the IMAGE_DIRECTORY location from preferences then display them in a JLabel
        try{
            JPanel gallery = new JPanel();
            JScrollPane vertical;
            GridLayout galGrid = new GridLayout(0, 5);
            gallery.setLayout(galGrid);
            galGrid.setHgap(GAP);
            galGrid.setVgap(GAP);

            //create panel for each file
            ArrayList <File> images = getImagesFromPrefs();
            String sessionKey = getNewSessionKey();
            for (int i = 0; i<images.size(); i++) {
                System.out.println(dropSuffix(images.get(i).getName()));
                //get and scale image
                Image scaled = getScaledImage(images.get(i));
                ImagePanel imagePanel = new ImagePanel(scaled, images.get(i).getName());
                gallery.add(imagePanel);
                gallery.setBackground(Color.WHITE);

                //create image Info
                ImageStore img = new ImageStore(images.get(i).toString());
                img.start(sessionKey);
                imagePanel.setImageInfo(img);
            }
            vertical = new JScrollPane(gallery);

            home.add(vertical);

        }catch (Exception e){
            System.out.println("Error!!! " + e.getMessage());
            e.printStackTrace(System.out);
        }
        //move up
        home.pack();
        home.setVisible(true);

    }


    private String getNewSessionKey() throws IOException{
        URL url = new URL("http://nova.astrometry.net/api/login");
        URLConnection uc = url.openConnection();
        uc.setDoOutput(true);
        uc.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        try(PrintWriter pw = new PrintWriter(uc.getOutputStream())){
            JSONObject obj = new JSONObject();
            obj.put("apikey", apiKey);
            pw.println("request-json=" + obj.toString());
        }
        StringBuilder sb = new StringBuilder();
        String res;
        try(BufferedReader in = new BufferedReader(new InputStreamReader(uc.getInputStream()))){
            res = in.readLine();
            sb.append(res);
            System.out.println(res);
        }
        return isolateSession(res);
    }

    /**
     * Janky as hell, can use Gson to convert to an object and get it that way.
     * @param out
     * @return session key from output string
     */
    private String isolateSession(String out){
        if (out.contains("session")){
            int index = out.indexOf("session");
            int endKeyIndex = out.indexOf('"', index+11);
            return out.substring(index+11, endKeyIndex);
        }else{
            return "";
        }
    }

    /**
     * Checks files at preferences location for images and returns all files that end in .jpg
     * TODO: modify to include other image file types
     * @return
     */
    private ArrayList<File> getImagesFromPrefs(){
        File imageFolder = new File (prefs.get(IMAGE_DIRECTORY, null));
        File[] files = imageFolder.listFiles();
        ArrayList <File> images = new ArrayList<>();
        for (int i = 0; i< files.length; i++){
            if(files[i].getName().toLowerCase().endsWith(".jpg"))
                images.add(files[i]);
        }
        return images;
    }


    private String dropSuffix(String s){
        for(int i = s.length()-1; i >=0; i--){
            if (s.charAt(i) == '.'){
                return s.substring(0, i);
            }
        }
        return s;
    }

    /**
     * @param f file found at preferences
     * @return scaled image to fit in SIZE
     */
    private Image getScaledImage(File f){
        try {
            BufferedImage image = ImageIO.read(f);
            int w = image.getWidth(null);
            int h = image.getHeight(null);
            float scaleW = (float) SIZE / w;
            float scaleH = (float) SIZE / h;

            //-ve number used as java will auto scale to maintain aspect ratio if -ve is given.
            if (scaleW > scaleH) {
                w = -1;
                h = (int) (h * scaleH);
            } else {
                w = (int) (w * scaleW);
                h = -1;
            }
            return image.getScaledInstance(w,h,Image.SCALE_SMOOTH);
        }catch (Exception e){
            e.printStackTrace(System.out);
        }
        return null;
    }


}
