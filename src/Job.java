import java.util.ArrayList;

/**
 * Class for using with GSon to get data from Json
 */
public class Job {
    private String processing_started;
    private ArrayList job_calibrations;
    private ArrayList <Integer> jobs;
    private String processing_finished;
    private int user;
    private ArrayList<Integer> user_images;

    public ArrayList getJobID(){
        return jobs;
    }

    public ArrayList getJobCalibrations(){
        return job_calibrations;
    }

    public int GetJobNumber(){
        return (int) jobs.get(0);
    }
}
