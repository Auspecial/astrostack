import javax.swing.*;

public class MenuBar extends JMenuBar{

    private JMenuBar menuBar;
    private JMenu menu;
    private SortMenu sortMenu;

    public MenuBar(){
        super();
        FileMenu file = new FileMenu("File");
        SettingsMenu settings = new SettingsMenu("Settings");
        sortMenu = new SortMenu("Sort");
        this.add(file);
        this.add(settings);
        this.add(sortMenu);
    }

}
