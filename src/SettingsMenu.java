import javax.swing.*;

public class SettingsMenu extends JMenu {

    public SettingsMenu(String name){
        super(name);
        this.add("Preferences");
        this.add("Return Defaults");
    }

}
