import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class SortMenu extends JMenu {

    protected JMenu fileMenu;

    public SortMenu(String name){
        super(name);
        ButtonGroup group = new ButtonGroup();

        ArrayList <JRadioButtonMenuItem> catalogues = new ArrayList<>();

        JRadioButtonMenuItem messier = new JRadioButtonMenuItem("Messier");
        JRadioButtonMenuItem caldwell = new JRadioButtonMenuItem("Caldwell");
        JRadioButtonMenuItem ngc = new JRadioButtonMenuItem("NGC");
        JRadioButtonMenuItem herschel = new JRadioButtonMenuItem("Herschel");

        catalogues.add(messier);
        catalogues.add(caldwell);
        catalogues.add(ngc);
        catalogues.add(herschel);

        for (JRadioButtonMenuItem item: catalogues){
            item.setSelected(false);
            group.add(item);
            add(item);
        }


    }

}