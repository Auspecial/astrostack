import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class DirectoryList {

    private ArrayList<String> dirList = new ArrayList<>();

    public DirectoryList(){

        //Need to check if file exists if not create a new file
        //Else do below
        String fileName = "DirectoryList.txt";
        String line;
        try {

            FileReader fileReader = new FileReader(fileName);

            BufferedReader bufferedReader = new BufferedReader(fileReader);

            while ((line = bufferedReader.readLine()) != null){
                System.out.println(line);
            }

            bufferedReader.close();

        } catch (FileNotFoundException e) {
            //e.printStackTrace();

            System.out.println("No directory list found");
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void addDirectory(String dir){

        dirList.add(dir);

    }

    public ArrayList<String> getDirectory(){
        return this.dirList;
    }
}
