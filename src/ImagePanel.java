import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 * Image Panel is about the visual and interactiveness of each image on the home page.
 * Mouse Listeners can return information about the image
 */
public class ImagePanel extends JPanel {

    private ImageIcon image;
    private String name;
    private int width;
    private int height;
    private final int SIZE = 250;
    private ImageStore imgStore;

    public ImagePanel(Image image, String name){
        JPanel imagePanel = new JPanel(new GridLayout(0,1)){
            @Override
            public Dimension getPreferredSize(){
                return new Dimension(SIZE, SIZE);
            }
        };
        imagePanel.setBackground(Color.WHITE);
        JLabel title = new JLabel(name);
        imagePanel.add(new JLabel(new ImageIcon(image)));
        imagePanel.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if(imgStore.analysisStatus()) {
                    JOptionPane.showMessageDialog(null, imgStore.getTagList());

                }else{
                    JOptionPane.showMessageDialog(null, "Image not yet plate solved please wait. This takes some time.");
                }
            }
            @Override
            public void mousePressed(MouseEvent e) { }
            @Override
            public void mouseReleased(MouseEvent e) {}
            @Override
            public void mouseEntered(MouseEvent e) {imagePanel.setBackground(Color.BLACK);}
            @Override
            public void mouseExited(MouseEvent e) {imagePanel.setBackground(Color.WHITE);}
        });

        this.add(imagePanel);

    }

    public void setImageInfo(ImageStore imageStore){
        this.imgStore = imageStore;
    }

    public String getImageTags(){
        return this.imgStore.getTags();
    }

    public void renameImage(String newName){
        this.name = newName;
    }
}
