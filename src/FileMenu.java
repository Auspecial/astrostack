import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.prefs.Preferences;

public class FileMenu extends JMenu {

    String  dirPath = "";
    DirectoryList directories = new DirectoryList();
    String IMAGE_DIRECTORY = "";


    public FileMenu(String name){
        super(name);
        final JFileChooser fc = new JFileChooser();
        String dir = "";
        fc.setCurrentDirectory(new java.io.File("."));
        fc.setDialogTitle(dir);
        fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        fc.setAcceptAllFileFilterUsed(false);


        JMenuItem addDirectory = new JMenuItem("Add Directory");
        addDirectory.addActionListener(actionEvent -> {

            int returnVal = fc.showOpenDialog(FileMenu.super.getComponent());
            if (returnVal == JFileChooser.APPROVE_OPTION){
                File file = fc.getSelectedFile();
                System.out.println(file.toString());
                directories.addDirectory(file.toString());
                System.out.println(directories.getDirectory());

                Preferences pref = Preferences.userNodeForPackage(Home.class);
                pref.put(IMAGE_DIRECTORY, file.toString());

            }

//                dirPath = JOptionPane.showInputDialog("Please input the path to the directory");
//                System.out.println(dirPath);

        });
        this.add(addDirectory);


        this.add("Remove Directory");


        JMenuItem quit = new JMenuItem("Quit");
        quit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                System.exit(0);
            }
        });


        this.add(quit);
    }
}
