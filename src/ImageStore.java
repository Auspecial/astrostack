import com.google.gson.Gson;
import org.apache.http.HttpEntity;
import org.apache.http.StatusLine;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.json.JSONObject;

import java.io.*;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Properties;

/**
 * This class is responsible for getting and storing data about each image.
 * Each ImagePanel class will have an ImageStore class associated with it.
 *
 * TODO
 * 1. if plate solve doesn't work timeout gracefully
 */

public class ImageStore implements Runnable{
    private String tags;
    private String fileLoc;
    private ArrayList<String> tagList;
    private boolean hasBeenAnalysed;
    private Thread thread;
    private String sessionKey;
    private boolean cals = false;
    Properties prop = new Properties();
    OutputStream output = null;

    //set uploading to true to start uploading images for testing
    private boolean uploading = true;


    /**
     * Open relevant properties file and check if it has been analysed before
     * @param fileLoc
     */
    public ImageStore(String fileLoc){
        this.fileLoc = fileLoc;
        System.out.println("Creating ImageStore for file at: " + fileLoc);
        Properties p = new Properties();
        try {
            InputStream in = new FileInputStream(fileLoc + ".properties");
            p.load(in);
            if(p.getProperty("analysed").equals("true")){
                System.out.println(fileLoc + " image had been analysed before");
                this.tags = p.getProperty("tags");
                this.tagList = getArrayFromTags(this.tags);
                this.hasBeenAnalysed = true;
                System.out.println("not sending due to prior analysis for " + fileLoc);
            }
        }catch(Exception e){
            System.out.println("No properties file detected. " + fileLoc);
        }
    }

    public void imageAnalysed(){
        this.hasBeenAnalysed = true;
    }

    public String getTags(){
        return this.tags;
    }

    public boolean analysisStatus(){
        return hasBeenAnalysed;
    }


    /**
     * If image has not been analysed before perform analiysis to get tags.
     */
    public void run() {
        try{
            System.out.println("Image has been analysed: " + hasBeenAnalysed);
            if(uploading && !hasBeenAnalysed) {
                output = new FileOutputStream(fileLoc + ".properties");
                System.out.println("Running...");
                String jobID = getJobID(sessionKey);
                this.tags = getTagsFromJob(jobID, sessionKey);
                this.tagList = getArrayFromTags(tags);
                this.hasBeenAnalysed = true;
                prop.setProperty("location", fileLoc);
                prop.setProperty("analysed", "true");
                prop.setProperty("tags", this.getTags());
                prop.store(output, null);
            }
        }catch(Exception e){
            e.printStackTrace(System.out);
        } finally {
            if (output != null) {
                try {
                    output.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }
        System.out.println("Exiting thread: " + fileLoc);
    }

    public void start(String sessionKey){
        this.sessionKey = sessionKey;
        System.out.println("Starting thread.");
        if (thread == null){
            thread = new Thread(this, fileLoc);
            thread.start();
        }
    }

    /**
     * Gets the job ID when an image analysis is being performed.
     * To be used when making subsequent API calls.
     * @param SK
     * @return
     * @throws Exception
     */
    private String getJobID(String SK) throws Exception{
        File image = new File(this.fileLoc);
        CloseableHttpClient client = HttpClients.createDefault();
        HttpPost post = new HttpPost("http://nova.astrometry.net/api/upload");
        JSONObject sessionObj = getUploadJSON(SK);

        //build and send request
        MultipartEntityBuilder builder = MultipartEntityBuilder.create();
        builder.addPart("request-json", new StringBody(sessionObj.toString(), "application/json", Charset.defaultCharset()));
        builder.addPart("file", new FileBody(image, image.toString(), "application/octet-stream", Charset.defaultCharset().toString()));
        HttpEntity multi = builder.build();
        post.setEntity(multi);
        CloseableHttpResponse response = client.execute(post);
        HttpEntity entity = response.getEntity();

        //deal with response
        StatusLine sl = response.getStatusLine();
        int status = sl.getStatusCode();
        System.out.println("status Code is: " + status);
        String result = null;
        if (status == 200) {
            StringBuilder sb = new StringBuilder();
            try (InputStream content = entity.getContent()) {
                BufferedReader reader = new BufferedReader(new InputStreamReader(content));
                String line;
                while ((line = reader.readLine()) != null) {
                    sb.append(line).append("\n");
                }
            }
            result = sb.toString();
        } else {
            System.out.println("Error! Status code is: " + status);
        }
        //client.close();
        System.out.println(result);
        Gson g = new Gson();
        Submission subID = g.fromJson(result, Submission.class);
        System.out.println("Submission ID is: " + subID.getSubID());

        String jobID = getJobIDFromSubmissionID(subID.getSubID());
        System.out.println("Job ID is: " + jobID);

        //String objects = getObjectsInJob(jobID, subID.getSubID());
        //System.out.println(objects);

        client.close();
        return jobID;

    }

    /**
     * creates a json object that needs to be passed with the initial upload request
     * @param session
     * @return
     */
    private JSONObject getUploadJSON(String session){
        JSONObject o = new JSONObject();
        o.put("publicly_visible", "y");
        o.put("allow_modifications", "d");
        o.put("session", session);
        o.put("allow_commercial_use", "d");
        return o;
    }

    /**
     * Helper method for getJobID()
     * @param subID
     * @return
     * @throws Exception
     */
    private String getJobIDFromSubmissionID(String subID) throws Exception{
        Job job = new Job();
        while (!cals) {
            CloseableHttpClient client = HttpClients.createDefault();
            HttpPost post = new HttpPost("http://nova.astrometry.net/api/submissions/" + subID);
            CloseableHttpResponse response = client.execute(post);
            HttpEntity ent = response.getEntity();

            StatusLine sl = response.getStatusLine();
            int status = sl.getStatusCode();
            System.out.println("status Code is: " + status);
            String result = null;
            if (status == 200) {
                StringBuilder sb = new StringBuilder();
                try (InputStream content = ent.getContent()) {
                    BufferedReader reader = new BufferedReader(new InputStreamReader(content));
                    String line;
                    while ((line = reader.readLine()) != null) {
                        sb.append(line).append("\n");
                    }
                }
                result = sb.toString();
            } else {
                System.out.println("Error! Get job ID from sub ID status code is: " + status);
            }
            client.close();
            System.out.println(result);

            Gson g = new Gson();
            job = g.fromJson(result, Job.class);
            if (job.getJobCalibrations().size() == 0) {
                System.out.println("Job calibration not done sleeping for 30s then restarting query.");
                Thread.sleep(30000);
            } else {
                cals = true;
            }

        }
        //System.out.println("Job ID is : " + job.getJobID()[0]);
        int jb = job.GetJobNumber();
        return Integer.toString(jb);
    }

    /**
     * Creates an API call to get the String of tags that have been analysed from the image.
     * @param jobID
     * @param sessionKey
     * @return
     * @throws Exception
     */
    private String getTagsFromJob(String jobID, String sessionKey) throws Exception{
        CloseableHttpClient client = HttpClients.createDefault();
        HttpPost post = new HttpPost("http://nova.astrometry.net/api/jobs/" + jobID + "/tags/");
        JSONObject sessionObj = getUploadJSON(sessionKey);

        //build and send request
        MultipartEntityBuilder builder = MultipartEntityBuilder.create();
        builder.addPart("session", new StringBody(sessionKey, "application/json", Charset.defaultCharset()));
        //builder.addPart("file", new FileBody(image, image.toString(), "application/octet-stream", Charset.defaultCharset().toString()));
        HttpEntity multi = builder.build();
        post.setEntity(multi);
        CloseableHttpResponse response = client.execute(post);
        HttpEntity ent = response.getEntity();


        StatusLine sl = response.getStatusLine();
        int status = sl.getStatusCode();
        String reason = sl.getReasonPhrase();
        System.out.println("status Code is: " + status);
        String result = null;
        if (status == 200) {
            StringBuilder sb = new StringBuilder();
            try (InputStream content = ent.getContent()) {
                BufferedReader reader = new BufferedReader(new InputStreamReader(content));
                String line;
                while ((line = reader.readLine()) != null) {
                    sb.append(line).append("\n");
                }
            }
            result = sb.toString();
        } else {
            System.out.println("Error! Get objects in job status code is: " + status);
            System.out.println("Reason: " + reason);
        }
        client.close();
        System.out.println(result);
        return result;
    }

    /**
     * Get an array of tags, from the tag string, uses Gson so a class is needed to make an
     * object from the Json string.
     * @param tag
     * @return
     */
    private ArrayList<String> getArrayFromTags(String tag){
        String snipped = tag;
        Gson g = new Gson();
        TagList tl = g.fromJson(snipped, TagList.class);
        return tl.getTags();
    }

    public ArrayList<String> getTagList(){
        return this.tagList;
    }

    private class TagList{
        ArrayList <String> tags;

        public ArrayList <String> getTags(){
            return tags;
        }
    }
}
