/**
 * Class for using with GSon to get data from Json
 */

public class Submission {
    private String status;
    private String subid;
    private String hash;

    public String getSubID(){
        return subid;
    }
}
